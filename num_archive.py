import zipfile, os

def zip_archive (dir):
    dir = os.path.abspath(dir)

    num = 1
    while True:
        zip_name = os.path.basename(dir) + "_" + str(num) + '.zip'
        if not os.path.exists(zip_name):
            break
        num = num + 1
    
    # Create the archive
    print('Archiving %s...' % (zip_name))
    archive_zip = zipfile.ZipFile(zip_name, 'w')

    # Directory walking
    for dir_name, sub_dir, file_names in os.walk(dir):
        print('Adding files %s...' % (dir_name))
        archive_zip.write(dir_name)

        # Add all the files in the directory to the archive    
        for file_name in file_names:
            if file_name.startswith(os.path.basename(dir) + '_') and file_name.endswith('.zip'):
                continue
            archive_zip.write(os.path.join(dir_name, file_name))
    
    # Close
    archive_zip.close()

    # Done
    print('Completed')

if __name__ == "__main__":
    # Test directory
    zip_archive('.\\arch_test')